"use client";

import Link from "next/link";
import UserVignette from "@/components/ui/user-vignette";

import { DynaPuff } from 'next/font/google';

const fascinate = DynaPuff({
  weight: ['400'],
  style: ['normal'],
  subsets: ['latin'],
  display: 'swap',
});

export default function MenuPage() {
  return (
    <div className="flex w-full h-full flex-col gap-32 px-32 pt-32 pb-16">
      <div className={`uppercase text-indigo-100 font-extrabold text-8xl pt-4 ${fascinate.className}`}>
        Blindtest'o'moot
      </div>

      <div className="flex grow flex-col-reverse">
        <div className="flex flex-col gap-4 items-start font-bold text-7xl">
          <Link href="/singleplayer" className="block hover:text-8xl hover:text-indigo-300 transition-all duration-150 ease-in-out">Singleplayer</Link>
          <Link href="/multiplayer" className="block hover:text-8xl hover:text-indigo-300 transition-all duration-150 ease-in-out">Multiplayer</Link>
          <Link href="/options" className="block hover:text-8xl hover:text-indigo-300 transition-all duration-150 ease-in-out">Settings</Link>
        </div>
      </div>

      <UserVignette />
    </div>
  );
}
