import "./globals.css";

import PageTransitionEffect from "./page-transition-effect";

import { Space_Grotesk } from 'next/font/google';

const space_grotesk = Space_Grotesk({
  weight: ['variable'],
  style: ['normal'],
  subsets: ['latin'],
  display: 'swap',
});

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={`bg-indigo-300 ${space_grotesk.className}`}>
        <div className="fixed w-10/12 h-full bg-gradient-to-br from-indigo-950 to-indigo-900 text-slate-50">
          <PageTransitionEffect>{children}</PageTransitionEffect>
        </div>
      </body>
    </html>
  );
}
