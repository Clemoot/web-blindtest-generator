"use client";

import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeftLong } from '@fortawesome/free-solid-svg-icons';
import UserVignette from '@/components/ui/user-vignette';

import { DynaPuff } from 'next/font/google';

const fascinate = DynaPuff({
    weight: ['400'],
    style: ['normal'],
    subsets: ['latin'],
    display: 'swap',
});

export default function SingleplayerPage() {
    return (
        <div className="flex w-full h-full flex-col gap-32 px-32 pt-32 pb-16">
            <div className={`uppercase text-indigo-100 font-extrabold text-8xl pt-4 ${fascinate.className}`}>
                Singleplayer
            </div>

            <div className="flex grow flex-col-reverse">
                <div className="flex flex-col gap-4 items-start font-bold text-7xl">
                    <Link href="/singleplayer/music" className="block hover:text-8xl hover:text-indigo-300 transition-all duration-150 ease-in-out">Music</Link>
                    <Link href="/singleplayer/movies" className="block hover:text-8xl hover:text-indigo-300 transition-all duration-150 ease-in-out">Films & series</Link>
                    <Link href="/singleplayer/anime" className="block hover:text-8xl hover:text-indigo-300 transition-all duration-150 ease-in-out">Anime</Link>
                </div>
                <Link href="/" className="block hover:text-indigo-300 transition-colors duration-150 ease-in-out mb-8"><FontAwesomeIcon icon={faArrowLeftLong} />  Return</Link>
            </div>

            <UserVignette />
        </div>
    );
}
