'use client';

import { motion } from "motion/react";

export default function Template({ children }: { children: React.ReactNode }) {
    return (
        <motion.div
            initial={{ y: 20, opacity: 0, height: "inherit" }}
            animate={{ y: 0, opacity: 1, height: "inherit" }}
            exit={{ y: 20, opacity: 0, height: "inherit" }}
            transition={{ ease: "easeInOut", duration: 0.75 }}
        >
            {children}
        </motion.div>
    );
}
