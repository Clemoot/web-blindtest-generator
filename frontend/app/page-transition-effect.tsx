'use client';

import { motion, AnimatePresence } from 'framer-motion';
import { usePathname } from 'next/navigation';
import { LayoutRouterContext } from 'next/dist/shared/lib/app-router-context.shared-runtime';
import { useContext, useRef } from 'react';

function FrozenRouter(props: { children: React.ReactNode }) {
    const context = useContext(LayoutRouterContext ?? {});
    const frozen = useRef(context).current;

    if (!frozen) {
        return <>{props.children}</>;
    }

    return (
        <LayoutRouterContext.Provider value={frozen}>
            {props.children}
        </LayoutRouterContext.Provider>
    );
}

const variants = {
    hidden: { opacity: 0, x: -20, scale: 0.95 },
    enter: { opacity: 1, x: 0, scale: 1 },
    exit: { opacity: 0, x: 100, scale: 1 },
};

const PageTransitionEffect = ({ children }: { children: React.ReactNode }) => {
    // The `key` is tied to the url using the `usePathname` hook.
    const key = usePathname();

    return (
        <AnimatePresence mode="popLayout">
            <motion.div
                key={key}
                initial="hidden"
                animate="enter"
                exit="exit"
                className="h-full"
                variants={variants}
                transition={{ ease: 'easeInOut', duration: 0.75 }}
            >
                <FrozenRouter>{children}</FrozenRouter>
            </motion.div>
        </AnimatePresence>
    );
};

export default PageTransitionEffect;
