"use client";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons';

export default function UserVignette() {
    return (<div className="flex flex-row justify-start">
        <a href="/user" className="group flex flex-row gap-4">
            <div className="relative rounded-full p-6 bg-stone-950 transition-colors duration-100 ease-in-out group-hover:bg-stone-900">
                <FontAwesomeIcon className="absolute top-1/2 left-1/2 translate-x-[-50%] translate-y-[-50%]" icon={faUser} />
            </div>
            <div className="flex flex-col h-full justify-center">
                <div className="font-bold group-hover:underline transition-colors duration-100 ease-in-out group-hover:text-indigo-200">Username</div>
                <div className="text-slate-400 text-xs transition-colors duration-100 ease-in-out group-hover:text-indigo-300">Subtitle</div>
            </div>
        </a>
    </div>)
}